= BigCollatz
jorge.aguilera@puravida-software.com

Implementation of the Collatz conjeture using BigUint numbers

[source,rust]
----
use bigcollatz::BigCollatz;

let c = BigCollatz::new(100.to_biguint().unwrap() );
for x in c{
    println!("n:{}, step:{}", x.curr, x.step);
    assert_eq!( x.curr > BigUint::one(), true );
}
----
